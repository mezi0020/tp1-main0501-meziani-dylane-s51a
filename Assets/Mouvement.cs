using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //�lements de code pour le mouvement translationnel pour faire avancer ou reculer le joueur
        
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.back * 0.01f);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.forward * 0.01f);
        }

        //sprint : on a besoin d'appuyer sur shift + sur la fl�che du haut pour avoir un vitesse sup�rieure de d�placement
        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(Vector3.forward * 0.1f);
        }

        //�lements de code pour le movement rotative pour faire tourner le joueur sur lui m�me dans un sens ou l�autre

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -2);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, 2);
        }

        // On fais le m�me raisonnement avec une translation comme pour les d�placements basiques.
        if(Input.GetKey(KeyCode.Space))
        {
            transform.Translate(Vector3.up * 0.25f);
        }
    }
}
